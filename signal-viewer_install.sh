#!/bin/bash
###########################################################
# Bilder scripts for starter scripts of Signal-viewer app #
# Author: Szokolai Tamas (HA71970) 			   #
# v0.4b 2021-04-01					   #
###########################################################
VERSION="#v0.3 beta 2021-03-30"
SFILE=smog_rtl_rx_tcp.sh 
CFILE=Start_Signal_viewer.sh
echo "Installing Signal-viewer..."

if [ -e $SFILE ]; then
	rm ./$SFILE
fi
if [ -e $CFILE ]; then
	rm ./$CFILE
fi

touch $SFILE
touch $CFILE

#Write server 
echo '#!/bin/bash' >> $SFILE
echo '#smogcli2 smog_rtl_rx iq transfer via tcp socket' >> $SFILE
echo '#Author: HA71970' >> $SFILE
echo $VERSION >> $SFILE
echo '' >> $SFILE
echo 'FILE=fifo' >> $SFILE
echo 'tcpaddr=127.0.0.1' >> $SFILE 
echo 'tcpport=1111' >> $SFILE
echo '#2021-022AJ' >> $SFILE
echo 'SAT_ID=47964' >> $SFILE
echo '' >> $SFILE
echo 'if [ -e "$FILE" ]; then' >> $SFILE
echo '    rm fifo' >> $SFILE
echo 'fi' >> $SFILE
echo 'mkfifo fifo' >> $SFILE
echo 'echo "Start smog_rtl_rx..."' >> $SFILE
echo './smog1_rtl_rx -g 50 -k -a -n -i $SAT_ID -O > fifo &' >> $SFILE
echo 'pidof smog1_rtl_rx' >> $SFILE
echo 'echo "Start filetotcp server..."' >> $SFILE
#Write client
echo '#!/bin/sh' >> $CFILE
echo '#Sinal-viewer starter script' >> $CFILE
echo '#Author: HA71970' >> $CFILE
echo $VERSION >> $CFILE
echo '' >> $CFILE
echo 'tcpaddr=127.0.0.1' >> $CFILE
echo 'tcpport=1111' >> $CFILE
echo '' >> $CFILE

case "$1" in
	-python3)
		#Configure Server Side (python3)
		echo "Configure client scripts... (python3)"
		echo 'cd ./signal-viewer/signal-viewer/scripts/python3' >> $SFILE
		echo 'python3 filetotcp.py $tcpaddr $tcpport ../../../../fifo' >> $SFILE
		echo 'pkill python3' >> $SFILE

		echo "Configure server scripts... (python3)"
		echo 'cd ./signal-viewer/signal-viewer/scripts/python3/' >> $CFILE
		echo 'python3 signalviewer.py $tcpaddr $tcpport' >> $CFILE
		STAT="ok"
	;;
	-python)
		#Configure Server Side (python)
		echo "Configure client scripts... (python)"
		echo 'cd ./signal-viewer/signal-viewer/scripts/python/' >> $SFILE
		echo 'python filetotcp.py $tcpaddr $tcpport ../../../../fifo' >> $SFILE
		echo 'pkill python' >> $SFILE

		echo "Configure server scripts... (python)"
		echo 'cd ./signal-viewer/signal-viewer/scripts/python/' >> $CFILE
		echo 'python signalviewer.py $tcpaddr $tcpport' >> $CFILE
		STAT='ok'
	;;
	-standalone-amd64)
		#Configure Server Side (standalone)
		echo "Configure client scripts... (standalone)"
		echo 'cd ./signal-viewer/signal-viewer/dist/amd64' >> $SFILE
		echo './filetotcp $tcpaddr $tcpport ../../../../fifo' >> $SFILE
		echo "Confifigure client scripts..."

		echo "Configure server scripts... (standalone)"
		echo 'cd ./signal-viewer/signal-viewer/dist/amd64' >> $CFILE
		echo './signalviewer $tcpaddr $tcpport' >> $CFILE
		STAT='ok'
	;;
	    *)
		echo "Usage: <-python>, <-python3>, <-standalone-amd64>"
		STAT=''
	;;
esac

if [[ -z $STAT ]]; then
    rm ./$SFILE	
    rm ./$CFILE	
    exit
fi

echo 'pkill smog1_rtl_rx' >> $SFILE
#echo 'pkill bash' >> $SFILE
echo 'exit' >> $SFILE
mv ./$SFILE ../
chmod +x ../$SFILE

#Configure Client Side
echo 'exit' >> $CFILE
mv ./$CFILE ../
chmod +x ../$CFILE

echo "Install completed...."
sleep 2
exit
