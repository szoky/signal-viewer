**Rövid leírás:**

A Signal-viewer programcsomag a SMOG-1, SMOG-P és ATL-1 műholdak követéséhez és az adatgyűjtéshez fejlesztett jelnézegető, amely a smogcli2 programcsomagra és a [gnuradio](https://wiki.gnuradio.org/index.php/Main_Page) blokkokra épül.

A smog_rtl_rx program 200ksps sebességű 32bit-es lebegőpontos komplex adatait jeleníti meg.

Választható megjelenítési módok: Vízesés, spektrum, time domain, konstelláció nézet.    

A programcsomag kliens-szerver felépítésű, így lehetőséget nyújt a távoli használatra. A smogcli2-t futtató RPi-n vagy PC-n a filetotcp(.py) program fut, amely a smog_rtl_rx_tcp.sh szkript által létrehozott fifo-ból olvassa az adatokat és a megadott ip/port-on továbbítja a Signal_viewer(.py) /QT5 alapú grafikus megjelenítő/ programnak. A nézegető program bármelyik gépre telepíthető, akár az interneten keresztül is használható.

Mivel a tcp kapcsolat nyílt, így az internetes használata rejt némi biztonsági kockázatot (ssh-ba irányítással javítható a helyzet). Ezen kívűl szükséges legalább 200kbps szabad adatátviteli sebesség, hogy elkerülhető legyen az adatveszteség.

A megjelenítő QtGUI és Python3 alapú. 

**Függöségek:**

GNU Radio Companion >= 3.10.0.0

Python 3.8.5

[GRC Telepítése](https://wiki.gnuradio.org/index.php/InstallingGR)

A csomag programjai standalone is futtathatók (futtatókörnyezetbe ágyazott előrefordított modulokkal), lásd a telepítésnél. 

**Telepítés:**

1. Lépjünk be az előzőleg telepített és működőképes smogcli2/build könyvtárba;  

2. Adjuk ki a 'git clone https://gitlab.com/szoky/signal-viewer' parancsot;

3. Lépjünk be a létrejött signal-viewer könyvtárba 'cd signal-viewer';

4. Futtassuk a 'bash signal-viewer_install.sh' telepítő szkriptet;
    
    A telpítőszkript az alábbi kapcsolókkal használható:
    
    **-python**

    Ha régebbi gnuradio van telepítve python2-vel akkor ez az opció hasznos lehet, Python2.7-re telepíti a szkripteket. Pl. RPi-hez.

    **-python3**

    Ha naprakész rendszert futtatunk, a fent megadott függőségekkel.

    **-standalone-amd64** (pillanatnyilag csak 64bites Linux-on megy)

    Nincs szükség a függőségek (QT5, gnuradio, Python3) telepítésére, a programok futtatókörnyezetben futnak. 

5. Lépjünk vissza a smogcli2/build könyvtárba 'cd ..'' 

6. Ha a nézegető programot localhost-on futtatjuk, nem kell tenni semmit, ha viszont távoli gépen, akkor a **smog_rtl_rx_tcp.sh** szkriptben a **tcpaddr** és **tcpport** változókba meg kell adni a szerver (RPi vagy PC az rtl stick-kel) IP címét és egy szabad portot
<alapértelmezett a **127.0.0.1:1111**>

7. A 6. pontnak megfelelően kell eljárni a **Start_Signal_viewer.sh** szkripttel kapcsolatban is. (Távoli konfiguráció esetén: A **tcpaddr** és **tcpport** változókba meg kell adni a szerver (RPi vagy PC az rtl szikkel) ip címét és portját
<alapértelmezett a **127.0.0.1:1111**>)

**Indítás:**

1. Szerver és a vevőprogram indításához a smogcli2/build direktoriban kiadni a **./smog_rtl_rx_tcp.sh** parancsot;

2. Kliens oldal (nézegető program) futtatásához a smogcli2/build direktoriban kiadni a **./Start_Signal_viewer.sh** parancsot.
